% vim: syntax=prolog
%------------------------------------------------------------------------------
% FICHEIRO: TESTES DE ADJUDICANTE ---------------------------------------------
%------------------------------------------------------------------------------

:- include('../adjudicante.pl').
:- include('../utils.pl').
:- include('../contrato.pl').

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ---------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Testam o sistema de inferência quanto à presença de um facto na base de -----
% conhecimento. ---------------------------------------------------------------
%------------------------------------------------------------------------------

testeFactoPositivoAdjudicante(1) :-
    demo(
        adjudicante(
            1,
            'Administracao Regional de Saude do Norte',
            503135593,
            'Portugal, Porto, Porto'
                  ),
        R),
    R == verdadeiro,
    write('Sucesso \n');
    write('Insucesso \n').

testeFactoPositivoAdjudicante(2) :-
    demo(
        adjudicante(
            2,
            'Camara Municipal de Portimao',
            505309939,
            'Portugal, Faro, Portimao'
                   ),
        R),
    R == verdadeiro,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência quanto à presença de um facto de conhecimento
% negativo na base de conhecimento. -------------------------------------------
%------------------------------------------------------------------------------

testeFactoNegativoAdjudicante(1) :-
    demo(
        adjudicante(
            1,
            'Municipio de Amarante',
            501102752,
            'Portugal, Porto, Amarante'
                   ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeFactoNegativoAdjudicante(2) :-
    demo(
        adjudicante(
            2,
            'Hospital Distrital de Santarem, E. P. E',
            506361462,
            'Portugal, Santarem, Santarem'
                   ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência perante conhecimento impreciso. --------------
%------------------------------------------------------------------------------
%
testeConhecimentoImpreciso(1) :-
    demo(
        adjudicante(
            5,
            'Centro Hospitalar e Universitario de Coimbra',
            124000000,
            'Coimbra, Portugal'
                   ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(2) :-
    demo(
        adjudicante(
            6,
            'Aguas do Norte',
            513606084,
            'Vila Real, Portugal'
                   ),
        R),
    demo(
        adjudicante(
            6,
            'Aguas do Norte, S.A.',
            513606084,
            'Vila Real, Portugal'
                   ),
        Y),
    demo(
        adjudicante(
            6,
            'Agu. Norte, S.A',
            513606084,
            'Vila Real, Portugal'
                   ),
        Z),
    R == desconhecido,
    Y == desconhecido,
    Z == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(3) :-
    demo(
        adjudicante(
            7,
            'Municipio de Arganil',
            506833232,
            'Braga'
                   ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(4) :-
    demo(
        adjudicante(
            89,
            'Municipio de Aveiro',
            505931192,
            'Aveiro, Portugal'
                   ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(5) :-
    demo(
        adjudicante(
            8,
            'Município de Setúbal',
            501294104,
            'Setubal, Portugal'
                   ),
        R),
    demo(
        adjudicante(
            8,
            'Município de Setúbal',
            129410450,
            'Setubal, Portugal'
                   ),
        Y),
    demo(
        adjudicante(
            88,
            'Município de Setúbal',
            501294104,
            'Setubal, Portugal'
                   ),
        Z),
    demo(
        adjudicante(
            88,
            'Município de Setúbal',
            129410450,
            'Setubal, Portugal'
                   ),
        Q),
    R == desconhecido,
    Y == desconhecido,
    Z == desconhecido,
    Q == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência perante conhecimento incerto. ----------------
%------------------------------------------------------------------------------

testeConhecimentoIncerto(1) :-
    demo(
        adjudicante(
            9,
            'Centro Hospitalar Universitario do Algarve',
            98275235,
            'Algarve, Portugal'
                   ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(2) :-
    demo(
        adjudicante(
            10,
            'Teste1',
            503657190,
            'Sintra,Lisboa,Portugal'
                   ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(3) :-
    demo(
        adjudicante(
            11,
            'Instituto da Mobilidade e dos Transportes',
            508195446,
            'Braga'
                   ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(4) :-
    demo(
        adjudicante(
            525324,
            'Instituto Nacional de Saude Doutor Ricardo Jorge',
            501427511,
            'Lisboa, Lisboa, Portugal'
                   ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência perante conhecimento interdito. --------------
%------------------------------------------------------------------------------

testeConhecimentoInterdito(1) :-
    demo(
        adjudicante(
            13,
            'Instituto Nacional de Estatística, I.P.',
            502237490,
            'Lisboa'
                   ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(2) :-
    demo(
        adjudicante(
            14,
            'nome de teste',
            503148768,
            'Evora, Evora, Portugal'
                   ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(3) :-
    demo(
        adjudicante(
            15,
            'Municipio de Loule',
            123456789,
            'Loule, Faro, Portugal'
                   ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(4) :-
    demo(
        adjudicante(
            999,
            'Centro de Formação Profissional da Indústria de Calçado'
                   ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam a evolução de conhecimento relativo ao predicado `adjudicante`. ------
%------------------------------------------------------------------------------

% Adicionar um adjudicante com nenhum dos campos que já esteja registado.
testeEvolucaoConhecimento(1) :-
    adicionar(
        adjudicante(
            101,
            'Aadlgnadlgandg',
            12415135135,
            'AFAfadfadg'
                   )
            ),
    write('Sucesso \n');
    write('Insucesso \n').

% Adicionar um adjudicante com um _IdAd_ que já se encontra registado.
testeEvolucaoConhecimento(2) :-
    adicionar(
        adjudicante(
            1,
            'maria',
            123154098,
            'adgpadga'
                   )
            ),
    write('Insucesso \n');
    write('Sucesso \n').

% Adicionar um adjudicante com um _Nome_ que já se encontra registado.
testeEvolucaoConhecimento(3) :-
    adicionar(
        adjudicante(
            102,
            'Centro Hospitalar do Oeste, E.P.E',
            123456789,
            'Lisboa'
                    )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

% Adicionar um adjudicante com um _NIF_ que já se encontra registado.
testeEvolucaoConhecimento(4) :-
    adicionar(
        adjudicante(
            103,
            'Teste',
            514993871,
            'Lisboa'
                   )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

%------------------------------------------------------------------------------
% Testam a involução de conhecimento relativo ao predico `adjudicante`. -------
%------------------------------------------------------------------------------

testeRemocaoConhecimento(1) :-
    remover(
        adjudicante(
            1,
            'maria',
            123154098,
            'cais_sodre'
                   )
           ),
    write('Insucesso \n');
    write('Sucesso \n').

run_tests(1) :-
    write('\n\n'),
    write('------------------------------------------------------------- \n'),
    write('|                  TESTES: ADJUDICANTE                      | \n'),
    write('------------------------------------------------------------- \n\n'),
    write('TESTES FACTOS POSITIVOS: \n'),
    write('________________________ \n\n'),
    testeFactoPositivoAdjudicante(1),
    testeFactoPositivoAdjudicante(2),
    write('\nTESTES FACTOS NEGATIVOS: \n'),
    write('________________________ \n\n'),
    testeFactoNegativoAdjudicante(1),
    testeFactoNegativoAdjudicante(2),
    write('\nTESTES CONHECIMENTO IMPRECISO: \n'),
    write('______________________________ \n\n'),
    testeConhecimentoImpreciso(1),
    testeConhecimentoImpreciso(2),
    testeConhecimentoImpreciso(3),
    testeConhecimentoImpreciso(4),
    testeConhecimentoImpreciso(5),
    write('\nTESTES CONHECIMENTO INCERTO: \n'),
    write('____________________________ \n\n'),
    testeConhecimentoIncerto(1),
    testeConhecimentoIncerto(2),
    testeConhecimentoIncerto(3),
    testeConhecimentoIncerto(4),
    write('\nTESTES CONHECIMENTO INTERDITO: \n'),
    write('______________________________ \n\n'),
    testeConhecimentoInterdito(1),
    testeConhecimentoInterdito(2),
    testeConhecimentoInterdito(3),
    testeConhecimentoInterdito(4),
    write('\nTESTE EVOLUCAO DE CONHECIMENTO: \n'),
    write('--------------------------------- \n\n'),
    testeEvolucaoConhecimento(1),
    testeEvolucaoConhecimento(2),
    testeEvolucaoConhecimento(3),
    testeEvolucaoConhecimento(4),
    write('\nTESTE REMOCAO DE CONHECIMENTO: \n'),
    write('-------------------------------- \n\n'),
    testeRemocaoConhecimento(1),
    write('\n\n').