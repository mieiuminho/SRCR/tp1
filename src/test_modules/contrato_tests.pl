% vim: syntax=prolog
%------------------------------------------------------------------------------
% FICHEIRO: TESTES DE CONTRATO ------------------------------------------------
%------------------------------------------------------------------------------

:- include('../contrato.pl').
:- include('../adjudicante.pl').
:- include('../adjudicataria.pl').
:- include('../utils.pl').

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ---------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Testam o sistema de inferência quanto à esperança quanto à presença de um ---
% facto na base de conhecimento. ----------------------------------------------
%------------------------------------------------------------------------------

testeFactoPositivoContrato(1) :-
    demo(
        contrato(
            1,
            1,
            1,
            'Aquisicao de Bens Moveis',
            'Concurso Publico',
            'AP 2250 - Aquisicao de Luvas',
            5416.20,
            241,
            'Portugal, Porto, Maia',
            (30,4,2020)
                 ),
        R),
    R == verdadeiro,
    write('Sucesso \n');
    write('Insucesso \n').

testeFactoPositivoContrato(2) :-
    demo(
        contrato(
            4,
            4,
            4,
            'Aquisicao de Bens Moveis',
            'Ajuste Direto',
            'Aquisicao de Equipamento',
            11500,
            15,
            'Portugal, Setubal, Alcacer do Sal',
            (12,12,2019)
                ),
        R),
    R == verdadeiro,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência perante conhecimento impreciso. --------------
%------------------------------------------------------------------------------

testeConhecimentoImpreciso(1) :-
    demo(
        contrato(
            5,
            8,
            31,
            'Aquisicao de Bens Moveis',
            'Concurso Publico',
            'compra de material variado',
            1500,
            300,
            'Barcelos, Braga, Portugal',
            (1,1,2020)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(2) :-
    demo(
        contrato(
            6,
            1,
            3,
            'Aquisicao de Servicos',
            'Ajuste Direto',
            'limpeza de ruas',
            5000,
            75,
            'Porto, Portugal',
            (3,4,2018)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').


testeConhecimentoImpreciso(3) :-
    demo(
        contrato(
            7,
            1,
            3,
            'Aquisicao de Servicos',
            'Concurso Publico',
            'limpeza de ruas',
            5000,
            75,
            'Porto, Portugal',
            (3,4,2018)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(4) :-
    demo(
        contrato(
            8,
            8,
            12,
            'Aquisicao de Bens Moveis',
            'Ajuste Direto',
            'compra de equipamento medico',
            42,
            90,
            'Barcelos, Braga, Portugal',
            (25,3,2020)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(5) :-
    demo(
        contrato(
            9,
            2,
            14,
            'Aquisicao de Servicos',
            'Concurso Publico',
            'compra de medicamentos',
            7500,
            50,
            'Braga,Portugal',
            (8,8,2018)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(6) :-
    demo(
        contrato(
            10,
            3,
            1,
            'Aquisicao de Servicos',
            'Ajuste Direto',
            'limpeza da via publica',
            5000,
            30,
            'Famalicao, Braga, Portugal',
            (14,9,2020)
                 ),
        R),
    demo(
        contrato(
            11,
            3,
            1,
            'Aquisicao de Servicos',
            'Ajuste Direto',
            'limpeza da via publica',
            5000,
            60,
            'Famalicao, Braga, Portugal',
            (14,9, 2020)
                 ),
        Y),
    R == desconhecido,
    Y == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência perante conhecimento incerto. ----------------
%------------------------------------------------------------------------------

testeConhecimentoIncerto(1) :-
    demo(
        contrato(
            12,
            13231513,
            17,
            'Aquisicao de Servicos',
            'Concurso Publico',
            'catering',
            1500,
            30,
            'Barcelos, Braga, Portugal',
            (27,7,2019)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(2) :-
    demo(
        contrato(
            11,
            21412,
            'Aquisicao de Bens Moveis',
            'Ajuste Direto',
            'compra de equipamente escolar',
            10000,
            90,
            'Coimbra, Portugal',
            (14,9,2020)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(3) :-
    demo(
        contrato(
            12,
            7,
            6,
            safasfa,
            'Concurso Publico',
            'diverso material',
            4515,
            90,
            'Beja, Portugal',
            (11,9,2019)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(4) :-
    demo(
        contrato(
            13,
            1,
            14,
            'Aquisicao de Servicos',
            dasgadga,
            'servicos de segurança',
            5250,
            30,
            'Braga, Portugal',
            (6,4,2019)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(5) :-
    demo(
        contrato(
            14,
            5,
            16,
            'Aquisicao de Bens Moveis',
            'Ajuste Direto',
            'diverso material',
            37,
            60,
            'Portimão, Algarve, Portugal',
            (5,2,2018)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(6) :-
    demo(
        contrato(
            15,
            2,
            6,
            'Aquisicao de Bens Moveis',
            'Ajuste Direto',
            'aquisicao de cobertura',
            10305,
            3253252,
            'Coimbra, Coimbra, Portugal',
            (3,4,2020)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(7) :-
    demo(
        contrato(
            16,
            4,
            15,
            'Aquisicao de Bens Moveis',
            'Consulta Previa',
            'CV000002.52020',
            521.78,
            287,
            'Evora, Portugal',
            (1,1,1)
                 ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência perante conhecimento interdito. --------------
%------------------------------------------------------------------------------

testeConhecimentoInterdito(1) :-
    demo(
        contrato(
            17,
            1,
            21,
            'Aquisicao de Bens Moveis',
            'Ajuste Direto',
            'aquisicao de 4000 doses de vacina contra a brucelose dos bovinos',
            9850.40,
            210,
            'Lisboa, Lisboa, Portugal',
            (25,3,2020)
                 ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(2) :-
    demo(
        contrato(
            18,
            9,
            1,
            'Aquisicao de Bens Moveis',
            'Ajuste Direto',
            'carga para maquina sutura',
            9600,
            90,
            'Viseu, Viseu, Portugal',
            (16,1,2020)
                 ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(3) :-
    demo(
        contrato(
            19,
            10,
            18,
            adgadg,
            'Consulta Previa',
            'meios de diagnostico nao radiologico',
            2320,
            339,
            'Madeira, Portugal',
            (27,1,2020)
                 ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(4) :-
    demo(
        contrato(
            20,
            8,
            15,
            'Aquisicao de Bens Moveis',
            asgadga,
            'ajuste directo n.º 86/DAC/2020',
            39985.49,
            60,
            'Portugal',
            (21,1,2020)
                 ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(5) :-
    demo(
        contrato(
            21,
            6,
            16,
            'Aquisicao',
            'Consulta Previa',
            '09LAT20',
            124124,
            323,
            'Porto, Porto, Portugal',
            (13,2,2020)
                 ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(6) :-
    demo(
        contrato(
            22,
            5,
            13,
            'Aquisicao de Bens Moveis',
            '2083/2020',
            183.24, 443,
            'Figueira da Foz, Coimbra, Portugal',
            (21,2,2020)
                 ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoInterdito(7) :-
    demo(
        contrato(
            23,
            4,
            14,
            'Aquisicao de Servicos',
            'Concurso Publico',
            'Assistência Técnica a veículos pesados da marca VOLVO',
            80000,
            365,
            'Moita, Setubal, Portugal',
            (1,1,1)
                 ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam a evolução de conhecimento relativo ao predicado `contrato`. ---------
%------------------------------------------------------------------------------

% Introducao de um contrato cujo número identificar já se encontra registado.
testeEvolucaoConhecimento(1) :-
    adicionar(
        contrato(
            1,
            1,
            1,
            'Bens e Servicos',
            'Ajuste Direto',
            'Teste 1',
            1,
            1,
            'Braga',
            (21, 12, 2020)
                )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

% Introdução de um contrato cujas entidades adjudicante e ajudicataria estao
% registadas. (Para contratos atribuídos por "Ajuste Direto").
% O contrato segue também a regra para contratos do tipo "Bens e Serviços" que
% determina que o valor tem de ser menor ou igaul a 75000.
testeEvolucaoConhecimento(2) :-
    adicionar(
        contrato(
            1001,
            1,
            1,
            'Bens e Servicos',
            'Ajuste Direto',
            'Teste 2',
            1,
            1,
            'Braga',
            (21, 12, 2020)
                 )
            ),
    write('Sucesso \n');
    write('Insucesso \n').

% Este contrato viola a regra dos contratos atribuídos por "Ajuste Direto" do
% tipo "Bens e Servicos que diz que o valor tem de ser, no máximo, 75000€.
testeEvolucaoConhecimento(3) :-
    adicionar(
        contrato(
            1002,
            1,
            1,
            'Bens e Servicos',
            'Ajuste Direto',
            'Teste 3',
            80000,
            1,
            'Braga',
            (21, 21, 2020)
                )
         ),
    write('Insucesso \n');
    write('Sucesso \n').

% Introdução de um contrato cujas entidades adjudicante e ajudicataria estao
% registadas. (Para contratos atribuídos por "Ajuste Direto").
% O contrato segue também a regra para contratos do tipo "Empreitada de Obras
% Publicas" que determina que o valor tem de ser menor ou igaul a 150000.
testeEvolucaoConhecimento(4) :-
    adicionar(
        contrato(
            1003,
            1,
            1,
            'Empreitada de Obras Publicas',
            'Ajuste Direto',
            'Teste 4',
            1,
            1,
            'Braga',
            (12, 12, 2020)
                 )
            ),
    write('Sucesso \n');
    write('Insucesso \n').

% Este contrato viola a regra dos contratos atribuídos por "Ajuste Direto" do
% tipo "Empreitada de Obras Publicas" que diz que o valor tem der ser, no
% máximo, 150000€.
testeEvolucaoConhecimento(5) :-
    adicionar(
        contrato(
            1004,
            1,
            1,
            'Empreitada de Obras Públicas',
            'Ajuste Direto',
            'Teste 5',
            150001,
            1,
            'Braga',
            (12, 12, 2020)
                )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

% Introdução de um contrato cujas entidades adjudicante e ajudicataria estao
% registadas. (Para contratos atribuídos por "Ajuste Direto").
% O contrato segue também a regra para contratos cujo tipo não é "Empreitada
% de Obras Públicas" nem "Bens e Servicos" que diz que o valor tem de ser,
% no máximo, 100000€.
testeEvolucaoConhecimento(6) :-
    adicionar(
        contrato(
            1005,
            1,
            1,
            'Aquisicao de Servicos',
            'Ajuste Direto',
            'Teste 6',
            1000,
            100,
            'Braga',
            (12, 12, 2020)
                 )
            ),
    write('Sucesso \n');
    write('Insucesso \n').

% Este contrato viola a regra dos contratos atribuídos por "Ajuste Direto" que
% não têm nenhum dos tipos acima referidos que diz que o valor pode ser, no
% máximo, 100000€.
testeEvolucaoConhecimento(7) :-
    adicionar(
        contrato(
            1006,
            1,
            1,
            'Aquisicao de Servicos',
            'Ajuste Direto',
            'Teste 7',
            101000,
            100,
            'Braga',
            (12, 12, 2020)
                 )
            ),
    write('Insucesso \n');
    write('Sucesso \n').

% Introdução de um contrato cujas entidades adjudicante e ajudicataria estao
% registadas. (Para contratos atribuídos por "Concurso Publico de Ambito
% Nacional"). O contrato segue também a regra para contratos do tipo
% "Empreitada de Obras Publicas" pelo que o valor não pode ultrapassar os
% 5225000€.
testeEvolucaoConhecimento(8) :-
    adicionar(
        contrato(
            1007,
            3,
            3,
            'Empreitada de Obras Publicas',
            'Concurso Publico de Ambito Nacional',
            'Teste 8',
            5000000,
            100,
            'Braga',
            (21, 12, 2020)
                 )
             ),
    write('Sucesso \n');
    write('Insucesso \n').

% Este contrato viola a regra dos contratos atribuídos por "Concurso Publico de
% Ambito Nacional" do tipo "Empreitada de Obras Publicas" que diz que não podem
% ter valor superior a 5225000€.
testeEvolucaoConhecimento(9) :-
    adicionar(
        contrato(
            1008,
            3,
            3,
            'Empreitada de Obras Publicas',
            'Concurso Publico de Ambito Nacional',
            'Teste 9',
            10000000,
            100,
            'Braga',
            (21, 12, 2020)
                 )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

% Introdução de um contrato cujas entidades adjudicante e ajudicataria estao
% registadas. (Para contratos atribuídos por "Concurso Publico de Ambito
% Nacional"). O contrato segue também a regra para contratos do tipo
% "Bens e Serviços" pelo que o valor não pode ultrapassar os 418000€.
testeEvolucaoConhecimento(10) :-
    adicionar(
        contrato(
            1009,
            2,
            2,
            'Bens e Servicos',
            'Concurso Publico de Ambito Nacional',
            'Teste 10',
            400000,
            100,
            'Braga',
            (21, 12, 2020)
                )
             ),
    write('Sucesso \n');
    write('Insucesso \n').

% Este contrato viola a regra dos contratos atribuídos por "Concurso Publico de
% Ambito Nacional" do tipo "Bens e Servicos" que diz que não podem ter valor
% superior a 418000€.
testeEvolucaoConhecimento(11) :-
    adicionar(
        contrato(
            1010,
            2,
            2,
            'Bens e Servicos',
            'Concurso Publico de Ambito Nacional',
            'Teste 11',
            500000,
            100,
            'Braga',
            (21, 12, 2020)
                 )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

% Este contrato diz respeito ao regime de "Ajuste Direto Simplificado" e
% respeita as regras por este impostas (_Valor_ <= 5000, _Prazo_ =< 365 e ser
% de um dos tipos descriminados no invariante).
testeEvolucaoConhecimento(12) :-
    adicionar(
        contrato(
            1011,
            2,
            3,
            'Aquisicao de Bens Moveis',
            'Ajuste Direto Simplificado',
            'Teste 12',
            4000,
            100,
            'Braga',
            (21, 12, 2020)
                 )
             ),
    write('Sucesso \n');
    write('Insucesso \n').

% Este contrato viola o valor máximo permitido nos contratos por "Ajuste Direto
% Simplificado".
testeEvolucaoConhecimento(13) :-
    adicionar(
        contrato(
            1012,
            2,
            3,
            'Locacao de Bens Moveis',
            'Ajuste Direto Simplificado',
            'Teste 13',
            6000,
            100,
            'Braga',
            (21, 12, 2020)
                 )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

% Este contrato viola o prazo máximo de um contrato atribuído por "Ajuste
% Direto Simpificado".
testeEvolucaoConhecimento(14) :-
    adicionar(
        contrato(
            1013,
            2,
            4,
            'Aquisicao de Servicos',
            'Ajuste Direto Simplificado',
            'Teste 14',
            4000,
            400,
            'Braga',
            (21, 12, 2020)
                 )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

% Não existe nenhum contrato celebrado entre `adjudicante: 1` e
% `adjudicataria:4` pelo que podemos testar a regra dos 3 anos introduzindo
% contratos do mesmo tipo nos últimos 3 anos de modo a que a soma passe os 75000
% €. Assim, o primeiro contrato a ser introduzido depois da soma igualar ou
% ultrapassar o valor vai ser recusado.
testeEvolucaoConhecimento(15) :-
    adicionar(
        contrato(
            1014,
            1,
            4,
            'Bens e Servicos',
            'Concurso Publico de Ambito Nacional',
            'Teste 15',
            50000,
            400,
            'Braga',
            (21, 12, 2020)
                )
             ),
    write('(Regra dos 3 anos): 1 inserido com sucesso \n');
    write('(Regra dos 3 anos): 1 nao inserido \n').

testeEvolucaoConhecimento(16) :-
    adicionar(
        contrato(
            1015,
            1,
            4,
            'Bens e Servicos',
            'Concurso Publico de Ambito Nacional',
            'Teste 16',
            30000,
            400,
            'Braga',
            (22, 12, 2020)
                )
             ),
    write('(Regra dos 3 anos): 2 inserido com sucesso \n');
    write('(Regra dos 3 anos): 2 nao inserido \n').

testeEvolucaoConhecimento(17) :-
    adicionar(
        contrato(
            1016,
            1,
            4,
            'Bens e Servicos',
            'Concurso Publico de Ambito Nacional',
            'Teste 17',
            1,
            400,
            'Braga',
            (24, 12, 2020)
                )
             ),
    write('Teste da regra dos 3 anos MAL sucedido \n');
    write('Teste da regra dos 3 anos BEM sucedido \n').


run_tests(1) :-
    write('\n\n'),
    write('------------------------------------------------------------- \n'),
    write('|                     TESTES: CONTRATO                      | \n'),
    write('------------------------------------------------------------- \n\n'),
        write('TESTES FACTOS POSITIVOS: \n'),
    write('________________________ \n\n'),
    testeFactoPositivoContrato(1),
    testeFactoPositivoContrato(2),
    write('\nTESTES CONHECIMENTO IMPRECISO: \n'),
    write('________________________________ \n\n'),
    testeConhecimentoImpreciso(1),
    testeConhecimentoImpreciso(2),
    testeConhecimentoImpreciso(3),
    testeConhecimentoImpreciso(4),
    testeConhecimentoImpreciso(5),
    testeConhecimentoImpreciso(6),
    write('\nTESTES CONHECIMENTO INCERTO: \n'),
    write('______________________________ \n\n'),
    testeConhecimentoIncerto(1),
    testeConhecimentoIncerto(2),
    testeConhecimentoIncerto(3),
    testeConhecimentoIncerto(4),
    testeConhecimentoIncerto(5),
    testeConhecimentoIncerto(6),
    testeConhecimentoIncerto(7),
    write('\nTESTES CONHECIMENTO INTERDITO: \n'),
    write('________________________________ \n\n'),
    testeConhecimentoInterdito(1),
    testeConhecimentoInterdito(2),
    testeConhecimentoInterdito(3),
    testeConhecimentoInterdito(4),
    testeConhecimentoInterdito(5),
    testeConhecimentoInterdito(6),
    testeConhecimentoInterdito(7),
    write('\nTESTES EVOLUCAO DE CONHECIMENTO: \n'),
    write('__________________________________ \n\n'),
    testeEvolucaoConhecimento(1),
    testeEvolucaoConhecimento(2),
    testeEvolucaoConhecimento(3),
    testeEvolucaoConhecimento(4),
    testeEvolucaoConhecimento(5),
    testeEvolucaoConhecimento(6),
    testeEvolucaoConhecimento(7),
    testeEvolucaoConhecimento(8),
    testeEvolucaoConhecimento(9),
    testeEvolucaoConhecimento(10),
    testeEvolucaoConhecimento(11),
    testeEvolucaoConhecimento(12),
    testeEvolucaoConhecimento(13),
    testeEvolucaoConhecimento(14),
    testeEvolucaoConhecimento(15),
    testeEvolucaoConhecimento(16),
    testeEvolucaoConhecimento(17),
    write('\n\n').