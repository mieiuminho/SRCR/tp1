%vim: syntax=prolog
%------------------------------------------------------------------------------
% FICHEIRO: TESTES DE ADJUDICATÁRIA -------------------------------------------
%------------------------------------------------------------------------------

:- include('../adjudicataria.pl').
:- include('../utils.pl').
:- include('../contrato.pl').

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ---------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Testam o sistema de inferência quanto à presença de um facto na base de -----
% conhecimento. ---------------------------------------------------------------
%------------------------------------------------------------------------------

testeFactoPositivoAdjudicataria(1) :-
    demo(
        adjudicataria(
            1,
            'RACLAC, LDA',
            507992822,
            'Portugal'
                     ),
        R),
    R == verdadeiro,
    write('Sucesso \n');
    write('Insucesso \n').

testeFactoPositivoAdjudicataria(2) :-
    demo(
        adjudicataria(
            2,
            'Porlagmedia - Edicao e Distribuicao, LDA',
            513023801,
            'Portugal'
                     ),
        R),
    R == verdadeiro,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência quanto à presença de um facto de conhecimento
% negativo na base de conhecimento. -------------------------------------------
%------------------------------------------------------------------------------

testeFactoNegativoAdjudicataria(1) :-
    demo(
        adjudicataria(
            1,
            'C.M.Babo - Auditoria & Gestao, Lda',
            504201190,
            'Portugal'
                     ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

testeFactoNegativoAdjudicataria(2) :-
    demo(
        adjudicataria(
            2,
            'Medicinalia Cormedica, Lda',
            500684324,
            'Portugal'
                     ),
        R),
    R == falso,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência perante conhecimento impreciso. --------------
%------------------------------------------------------------------------------

testeConhecimentoImpreciso(1) :-
    demo(
        adjudicataria(
            11,
            'Overpharma',
            505500001,
            'Portugal'
                     ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(2) :-
    demo(
        adjudicataria(
            12,
            'ESSITY PORTUGAL, LDA',
            503237612,
            'Avenida D. João II, Vila Nova de Gaia, Portugal'
                     ),
        R),
    demo(
        adjudicataria(
            12,
            'ESSITY PORTUGAL',
            503237612,
            'Avenida D. João II, Vila Nova de Gaia, Portugal'
                     ),
        Y),
    demo(
        adjudicataria(
            12,
            'ESSITY',
            503237612,
            'Avenida D. João II, Vila Nova de Gaia, Portugal'
                     ),
        Z),
    R == desconhecido,
    Y == desconhecido,
    Z == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(3) :-
    demo(
        adjudicataria(
            13,
            'Alvarsol, Lda',
            5,
            'Quarteira, Faro, Portugal'
                     ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(4) :-
    demo(
        adjudicataria(
            94,
            'João Antunes Amaro, Lda',
            500149003,
            'Portugal'
                    ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoImpreciso(5) :-
    demo(
        adjudicataria(
            14,
            'B. BRAUN MEDICAL, LDA',
            500153370,
            'Lisboa,Portugal'
                     ),
        R),
    demo(
        adjudicataria(
            14,
            'JOHNSON & JOHNSON MEDICAL',
            501506543,
            'Lisboa,Portugal'
                     ),
        Y),
    demo(
        adjudicataria(
            14,
            'JOHNSON & JOHNSON MEDICAL',
            500153370,
            'Lisboa,Portugal'
                     ),
        Z),
    R == desconhecido,
    Y == desconhecido,
    Z == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam o sistema de inferência perante conhecimento incerto. ----------------
%------------------------------------------------------------------------------

testeConhecimentoIncerto(1) :-
    demo(
        adjudicataria(
            15,
            'wutever',
            980166705,
            'Portugal'
                     ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(2) :-
    demo(
        adjudicataria(
            16,
            'Teva',
            135135135,
            'Portugal'
                     ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(3) :-
    demo(
        adjudicataria(
            17,
            'KNOW-HOW, LDA.',
            509884296,
            'Lisboa'
                     ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

testeConhecimentoIncerto(4) :-
    demo(
        adjudicataria(
            90,
            'Seguradoras Unidas, S.A.',
            500940231,
            'Portugal'
                     ),
        R),
    R == desconhecido,
    write('Sucesso \n');
    write('Insucesso \n').

%------------------------------------------------------------------------------
% Testam a evolução de conhecimento relativo ao predicado `adjudicataria`. ----
%------------------------------------------------------------------------------

% Adicionar uma adjudicataria com nenhum dos campos que já esteja registado.
testeEvolucaoConhecimento(1) :-
    adicionar(
        adjudicataria(
            101,
            'addgadg',
            214124124,
            'kadjbgadgjbadg'
                     )
            ),
    write('Sucesso \n');
    write('Insucesso \n').

% Adicionar uma adjudicataria com um _IdAda_ que já se encontra registado.
testeEvolucaoConhecimento(2) :-
    adicionar(
        adjudicataria(
            1,
            'claudia',
            124141412,
            'dsbgsdogbsd'
                     )
            ),
    write('Insucesso \n');
    write('Sucesso \n').

% Adicionar uma adjudicataria com um _Nome_ que já se encontra registado.
testeEvolucaoConhecimento(3) :-
    adicionar(
        adjudicataria(
            102,
            'ULTRASONO RADIOLOGIA, LDA',
            1235667654,
            'Lisboa'
                    )
             ),
    write('Insucesso \n');
    write('Sucesso \n').

% Adicionar uma adjudicataria com um _NIF_ que já se encontra registado.
testeEvolucaoConhecimento(4) :-
    adicionar(
        adjudicataria(
            103,
            'dagdakngadgnad',
            502818093,
            'Lisboa'
                     )
            ),
    write('Insucesso \n');
    write('Sucesso \n').

%------------------------------------------------------------------------------
% Testam a involução de conhecimento relativo ao predicado `adjudicataria`. ---
%------------------------------------------------------------------------------
testeRemocaoConhecimento(1) :-
    remover(
        adjudicataria(
            1,
            'claudia',
            124141412,
            'tiabes'
                     )
            ),
    write('Insucesso \n');
    write('Sucesso \n').

run_tests(1) :-
    write('\n\n'),
    write('------------------------------------------------------------- \n'),
    write('|                TESTES: ADJUDICATARIA                      | \n'),
    write('------------------------------------------------------------- \n\n'),
    write('TESTES FACTOS POSITIVOS: \n'),
    write('________________________ \n\n'),
    testeFactoPositivoAdjudicataria(1),
    testeFactoPositivoAdjudicataria(2),
    write('\nTESTES FACTOS NEGATIVOS: \n'),
    write('__________________________ \n\n'),
    testeFactoNegativoAdjudicataria(1),
    testeFactoNegativoAdjudicataria(2),
    write('\nTESTES CONHECIMENTO IMPRECISO: \n'),
    write('________________________________ \n\n'),
    testeConhecimentoImpreciso(1),
    testeConhecimentoImpreciso(2),
    testeConhecimentoImpreciso(3),
    testeConhecimentoImpreciso(4),
    testeConhecimentoImpreciso(5),
    write('\nTESTES CONHECIMENTO INCERTO: \n'),
    write('______________________________ \n\n'),
    testeConhecimentoIncerto(1),
    testeConhecimentoIncerto(2),
    testeConhecimentoIncerto(3),
    testeConhecimentoIncerto(4),
    write('\nTESTES DE EVOLUCAO DO CONHECIMENTO: \n'),
    write('_____________________________________ \n\n'),
    testeEvolucaoConhecimento(1),
    testeEvolucaoConhecimento(2),
    write('\nTESTES DE REMOCAO DE CONHECIMENTO: \n'),
    write('____________________________________ \n\n'),
    testeRemocaoConhecimento(1),
    write('\n\n').