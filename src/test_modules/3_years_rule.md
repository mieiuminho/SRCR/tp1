# Testes relativos à regra dos 3 anos

## Teste 1

```prolog
adicionar(contrato(1000, 1, 1, 'Teste 1', 'Concurso Publico', 'Teste 1', 100000, 241, 'Portugal', (1, 5, 2020))).

adicionar(contrato(1001, 1, 1, 'Teste 1', 'Concurso Publico', 'Teste 1', 1, 241, 'Portugal', (1, 5, 2020))).
```

Para que o teste seja bem sucedido os _outputs_ devem ser:

```
yes
no
```

## Teste 2

```prolog
adicionar(contrato(2000, 2, 2, 'Teste 2', 'Tipo de Teste', 'Tipo de Teste', 50000, 241, 'Portugal', (2, 2, 2018))).

adicionar(contrato(2001, 2, 2, 'Teste 2', 'Tipo de Teste', 'Tipo de Teste', 30000, 241, 'Portugal', (3, 2, 2018))).

adicionar(contrato(2002, 2, 2, 'Teste 2', 'Tipo de Teste', 'Tipo de Teste', 1, 241, 'Portugal', (4, 2, 2019))).
```

## Teste 3

```prolog
adicionar(contrato(3000, 3, 3, 'Teste 3', 'Teste 3', 'Teste 3', 60000, 300, 'Portugal', (1, 1, 2001))).

adicionar(contrato(3001, 3, 3, 'Teste 3', 'Teste 3', 'Teste 3', 30000, 300, 'Portugal', (2, 1, 2001))).

adicionar(contrato(3002, 3, 3, 'Teste 3', 'Teste 3', 'Teste 3', 10, 300, 'Portugal', (3, 1, 2001))).
```
