% vim: syntax=prolog
%------------------------------------------------------------------------------
% FICHEIRO: CONTRATOS ---------------------------------------------------------
%------------------------------------------------------------------------------

:- include('utils.pl').
:- dynamic contrato/10.

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ---------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- discontiguous contrato/10.
:- discontiguous excecao/1.
:- discontiguous nuloIncerto/1.
:- discontiguous nuloInterdito/1.
:- discontiguous nao/1.
:- discontiguous (::)/2.
:- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Representação de conhecimento positivo --------------------------------------
%------------------------------------------------------------------------------

contrato(1,
         1,
         1,
         'Aquisicao de Bens Moveis',
         'Concurso Publico',
         'AP 2250 - Aquisicao de Luvas',
         5416.20,
         241,
         'Portugal, Porto, Maia',
         (30,4,2020)
         ).

contrato(2,
         2,
         2,
         'Aquisicao de Servicos',
         'Ajuste Direto',
         'Aquisicao de Servicos de Publicidade',
         6000,
         244,
         'Portugal, Faro, Portimao',
         (29,4,2020)
         ).

contrato(3,
         3,
         3,
         'Aquisicao de Servicos',
         'Ajuste Direto',
         'MCDTs ao exterior do mes de Novembro',
         7502.22,
         30,
         'Portugal, Leiria, Caldas da Rainha',
         (30,11,2019)
         ).

contrato(4,
         4,
         4,
         'Aquisicao de Bens Moveis',
         'Ajuste Direto',
         'Aquisicao de Equipamento',
         11500,
         15,
         'Portugal, Setubal, Alcacer do Sal',
         (12,12,2019)
         ).

%------------------------------------------------------------------------------
% Representação de conhecimento negativo --------------------------------------
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
% Definição de conhecimento negativo ------------------------------------------
%------------------------------------------------------------------------------

-contrato(Id, IdAd, IdAda, TPross, TProced, Descricao, Valor, Prazo, Local, Data) :-
    nao(contrato(Id, IdAd, IdAda, TPross, TProced, Descricao, Valor, Prazo, Local, Data)),
    nao(excecao(contrato(Id, IdAd, IdAda, TPross, TProced, Descricao, Valor, Prazo, Local, Data))).

%------------------------------------------------------------------------------
% IMPRECISO -------------------------------------------------------------------
%------------------------------------------------------------------------------

% Não sabemos o id da entidade adjudicante do contrato realizado pela entidade
% adjudicataria de id = 31, assinado a 1 de Janeiro de 2020 e com prazo de 300
% dias. Sabemos apenas que toma valores entre 7 e 14
excecao(
    contrato(5,
            ID,
            31,
            'Aquisicao de Bens Moveis',
            'Concurso Publico',
            'compra de material variado',
            1500,
            300,
            'Barcelos, Braga, Portugal',
            (1,1,2020)
            )
        ) :- ID =< 14, ID >= 7.

% Não sabemos o Tipo de Procedimento do contrato celebrado entre a entidade
% adjudicante com id = 1 e a entidade adjudicantária com id = 3.
% O contrato foi assinado no dia 3 de Abril de 2018, com valor de 5000 euros,
% com prazo de 75 dias. O local a ser realizado é "Porto, Portugal".
% Trata-se de uma aquisicao de servicos, e tem como descrição "limpeza de ruas"
% Sabemos que é Ajuste Direto ou Concurso Publico.
excecao(
    contrato(6,
             1,
             3,
             'Aquisicao de Servicos',
             'Ajuste Direto',
             'limpeza de ruas',
             5000,
             75,
             'Porto, Portugal',
             (3,4,2018)
             )
        ).

excecao(
    contrato(7,
             1,
             3,
             'Aquisicao de Servicos',
             'Concurso Publico',
             'limpeza de ruas',
             5000,
             75,
             'Porto, Portugal',
             (3,4,2018)
             )
        ).

% Não sabemos o valor de um contrato entre as entidades com ids 8 e 12, sabemos
% que o valor não é 50000.
% É do tipo aquisicao_bens_moveis por ajuste_direto, tem prazo de 90 dias, é
% para Barcelos e tem como descrição "compra de equipamento medico".
excecao(
    contrato(8,
             8,
             12,
             'Aquisicao de Bens Moveis',
             'Ajuste Direto',
             'compra de equipamento medico',
             VALOR,
             90,
             'Barcelos, Braga, Portugal',
             (25,3,2020)
             )
        ) :- VALOR \= 50000.

% Não sabemos o prazo do contrato assinado dia 8 de Agosto de 2018, pelas
% entidades 2 e 14. Sabemos que é entre 30 e 60 dias ou entre 90 e 120 dias.
excecao(
    contrato(9,
             2,
             14,
             'Aquisicao de Servicos',
             'Concurso Publico',
             'compra de medicamentos',
             7500,
             PRAZO,
             'Braga,Portugal',
             (8,8,2018)
            )
        ) :- PRAZO =< 60 , PRAZO >= 30;
             PRAZO =< 120 , PRAZO >= 90.

% Não é conhecido nem o valor nem o prazo do contrato saldado pelas entidades 3
% e 1, no dia 14 de Setembro de 2020.
% Aquisicao de servicos por ajuste direto com descrição: "limpeza da via
% publica". Sabemos que o valor está entre 2500 e 7500 euros e que o prazo é 30
% ou 60 dias.
excecao(
    contrato(10,
             3,
             1,
             'Aquisicao de Servicos',
             'Ajuste Direto',
             'limpeza da via publica',
             VALOR,
             30,
             'Famalicao, Braga, Portugal',
             (14,9,2020)
            )
        ) :- VALOR =< 7500 , VALOR >= 2500.

excecao(
    contrato(11,
             3,
             1,
             'Aquisicao de Servicos',
             'Ajuste Direto',
             'limpeza da via publica',
             VALOR,
             60,
             'Famalicao, Braga, Portugal',
             (14,9,2020)
            )
        ) :- VALOR =< 7500 , VALOR >= 2500.


%------------------------------------------------------------------------------
% INCERTO ---------------------------------------------------------------------
%------------------------------------------------------------------------------

% Não conhecemos o id da entidade adjudicante do contrato com as seguintes
% informações:
% * id adjudicataria : 17
% * Tipo: aquisicao_servicos
% * Tipo Procedimento: concurso_publico
% * Desc: "catering"
% * Valor: 1500
% * Prazo: 30
% * Local: "Barcelos, Braga, Portugal"
% * Data: 27/07/2019
contrato(
    12,
    id1desc,
    17,
    'Aquisicao de Servicos',
    'Concurso Publico',
    'catering',
    1500,
    30,
    'Barcelos, Braga, Portugal',
    (27,7,2019)
    ).

nuloIncerto(id1desc).

excecao(
    contrato(IDC,
             _,
             ID2,
             TIPO,
             PROC,
             DESC,
             VALOR,
             PRAZO,
             LOCAL,
             D
             )
        ) :- contrato(IDC, id1desc, ID2, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, D).

% Não conhecemos o id da entidade adjudicataria do contrato com as seguintes
% informações:
% * id adjudicante: 11
% * Tipo: aquisicao_bens_moveis
% * Tipo Procedimento: ajuste_direto
% * Desc: "compra de equipamente escolar"
% * Valor: 10000
% * Prazo: 90
% * Local: "Coimbra, Portugal"
% * Data: 14/09/2020
contrato(
    11,
    id2desc,
    'Aquisicao de Bens Moveis',
    'Ajuste Direto',
    'compra de equipamente escolar',
    10000,
    90,
    'Coimbra, Portugal',
    (14,9,2020)
    ).

nuloIncerto(id2desc).

excecao(
    contrato(ID1,
             _,
             TIPO,
             PROC,
             DESC,
             VALOR,
             PRAZO,
             LOCAL,
             D
             )
        ) :- contrato(ID1, id2desc, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, D).

% Não conhecemos o Tipo do contrato saldado entre a entidade adjudicante 7 e a
% entidade adjudicataria 6.
% * Proc: concurso_publico
% * Desc: "diverso material"
% * Valor: 4515
% * Prazo: 90
% * Local: "Beja, Portugal"
% * Data: 11/09/2019
contrato(
    12,
    7,
    6,
    tipodesc,
    'Concurso Publico',
    'diverso material',
    4515,
    90,
    'Beja, Portugal',
    (11,9,2019)
    ).

nuloIncerto(tipodesc).

excecao(
    contrato(IDC,
             ID1,
             ID2,
             _,
             PROC,
             DESC,
             VALOR,
             PRAZO,
             LOCAL,
             D
             )
        ):- contrato(IDC, ID1, ID2, tipodesc, PROC, DESC, VALOR, PRAZO, LOCAL, D).

% Não é conhecido o tipo de procedimento do contrato saldado entre a ent.
% adjudicante 1 e a ent. adjudicataria 14.
% * Tipo: aquisicao_servicos
% * Desc: "servicos de segurança"
% * Valor: 5250
% * Prazo: 30
% * Local: "Braga, Portugal"
% * Data: 06/04/2019
contrato(
    13,
    1,
    14,
    'Aquisicao de Servicos',
    procdesc,
    'servicos de segurança',
    5250,
    30,
    'Braga, Portugal',
    (6,4,2019)
    ).

nuloIncerto(procdesc).

excecao(
    contrato(IDC,
             ID1,
             ID2,
             TIPO,
             _,
             DESC,
             VALOR,
             PRAZO,
             LOCAL,
             D
             )
        ):- contrato(IDC, ID1, ID2, TIPO, procdesc, DESC, VALOR, PRAZO, LOCAL, D).

% Não sabemos o valor do contrato saldado entre as entidades 5 e 16
% * Tipo: aquisicao_bens_moveis
% * Proc: ajuste_direto
% * DESC: "diverso material"
% * Prazo: 60
% * Local: "Portimão, Algarve, Portugal"
% * Data: 05/02/2018
contrato(
    14,
    5,
    16,
    'Aquisicao de Bens Moveis',
    'Ajuste Direto',
    'diverso material',
    valordesc,
    60,
    'Portimão, Algarve, Portugal',
    (5,2,2018)
    ).

nuloIncerto(valordesc).

excecao(
    contrato(IDC,
             ID1,
             ID2,
             TIPO,
             PROC,
             DESC,
             _,
             PRAZO,
             LOCAL,
             D
             )
        ):- contrato(IDC, ID1, ID2, TIPO, PROC, DESC, valordesc, PRAZO, LOCAL, D).

% Não sabemos o prazo do contrato saldado entre as entidades 2 e 6
% * TIPO: aquisicao_bens_moveis
% * Proc: ajuste_direto
% * Desc: "aquisicao de cobertura"
% * VALOR: 10305
% * Local: "Coimbra, Coimbra, Portugal"
% * Data: 03/04/2020

contrato(
    15,
    2,
    6,
    'Aquisicao de Bens Moveis',
    'Ajuste Direto',
    'aquisicao de cobertura',
    10305,
    prazodesc,
    'Coimbra, Coimbra, Portugal',
    (3,4,2020)
).

nuloIncerto(prazodesc).

excecao(
    contrato(IDC,
             ID1,
             ID2,
             TIPO,
             PROC,
             DESC,
             VALOR,
             _,
             LOCAL,
             D
             )
        ):- contrato(IDC, ID1, ID2, TIPO, PROC, DESC, VALOR, prazodesc, LOCAL, D).

% Não conhecemos a data de assinatura do contrato entre as entidades 4 e 15
% * Tipo: aquisicao_bens_moveis
% * PROC: consulta_previa
% * Desc: "CV000002.52020"
% * VALOR: 521.78
% * PRAZO: 287
% * Local: "Evora, Portugal"
contrato(
    16,
    4,
    15,
    'Aquisicao de Bens Moveis',
    'Consulta Previa',
    'CV000002.52020',
    521.78,
    287,
    'Evora, Portugal',
    datadesc
).

nuloIncerto(datadesc).

excecao(
    contrato(IDC,
             ID1,
             ID2,
             TIPO,
             PROC,
             DESC,
             VALOR,
             PRAZO,
             LOCAL,
             _
             )
        ):- contrato(IDC, ID1, ID2, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, datadesc).

%------------------------------------------------------------------------------
% INTERDITO -------------------------------------------------------------------
%------------------------------------------------------------------------------


% É impossivel saber o id da entidade adjudicante do contrato
% * id adjudicataria: 21
% * TIPO: aquisicao_bens_moveis
% * PROC: ajuste_direto
% * DESC: "aquisicao de 4.000 doses de vacina contra a brucelose dos bovinos"
% * VALOR: 9850.40
% * PRAZO: 210
% * Local: "Lisboa, Lisboa, Portugal"
% * Data: 25/03/2020
contrato(
    17,
    id1inter,
    21,
    'Aquisicao de Bens Moveis',
    'Ajuste Direto',
    'aquisicao de 4000 doses de vacina contra a brucelose dos bovinos',
    9850.40,
    210,
    'Lisboa, Lisboa, Portugal',
    (25,3,2020)
).

nuloInterdito(id1inter).

nao(contrato(IDC, _, ID2, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, D)) :-
    contrato(IDC, id1inter, ID2, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, D).

% É impossivel saber o id da entidade adjudicataria do contrato
% * id adjudicante: 9
% * TIPO: aquisicao_bens_moveis
% * PROC: ajuste_direto
% * DESC: "carga para maquina sutura"
% * VALOR: 9600
% * PRAZO: 90
% * LOCAL: "Viseu, Viseu, Portugal"
% * DATA: 16/01/2020
contrato(
    18,
    9,
    id2inter,
    'Aquisicao de Bens Moveis',
    'Ajuste Direto',
    'carga para maquina sutura',
    9600,
    90,
    'Viseu, Viseu, Portugal',
    (16,1,2020)
).

nuloInterdito(id2inter).

nao(contrato(IDC, ID1, _, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, D)) :-
    contrato(IDC, ID1, id2inter, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, D).

% É impossivel saber o tipo do contrato saldado entre as entidades 10 e 18
% * PROC: consulta_previa
% * DESC: "meios de diagnostico nao radiologico"
% * VALOR: 2320
% * PRAZO: 339
% * Local: "Madeira, Portugal"
% * DATA: 27/01/2020
contrato(19,
         10,
         18,
         tipointer,
         'Consulta Previa',
         'meios de diagnostico nao radiologico',
         2320,
         339,
         'Madeira, Portugal',
         (27,1,2020)
).

nuloInterdito(tipointer).

nao(contrato(IDC, ID1, ID2, _, PROC, DESC, VALOR, PRAZO, LOCAL, D)) :-
    contrato(IDC, ID1, ID2, tipointer, PROC, DESC, VALOR, PRAZO, LOCAL, D).

% É impossivel saber o tipo de procedimento do contrato saldado entre 8 e 15
% * Tipo: aquisicao_bens_moveis
% * DESC: "ajuste directo n.º 86/DAC/2020"
% * VALOR: 39985.49
% * PRAZO: 60
% * Local:"Portugal"
% * DATA: 21/01/2020
contrato(20,
         8,
         15,
         'Aquisicao de Bens Moveis',
         procinter,
         'ajuste directo n.º 86/DAC/2020',
         39985.49,
         60,
         'Portugal',
         (21,1,2020)
).

nuloInterdito(procinter).

nao(contrato(IDC, ID1, ID2, TIPO, _, DESC, VALOR, PRAZO, LOCAL, D)) :-
    contrato(IDC, ID1, ID2, TIPO, procinter, DESC, VALOR, PRAZO, LOCAL, D).

% É impossivel saber o valor exato do contrato saldado entre as entidades 6 e 16
% * TIPO: aquisicao_bens_moveis
% * PROC: consulta_previa
% * DESC: "09LAT20"
% * PRAZO: 323
% * LOCAL: "Porto, Porto, Portugal"
% * DATA: 13/02/2020
contrato(21,
         6,
         16,
         'Aquisicao',
         'Consulta Previa',
         '09LAT20',
         valorinter,
         323,
         'Porto, Porto, Portugal',
         (13,2,2020)
).

nuloInterdito(valorinter).

nao(contrato(IDC, ID1, ID2, TIPO, PROC, DESC, _, PRAZO, LOCAL, D)) :-
    contrato(IDC, ID1, ID2, TIPO, PROC, DESC, valorinter, PRAZO, LOCAL, D).

% É impossivel saber o prazo do contrato saldado entre as entidades 5 e 13
% * TIPO: aquisicao_bens_moveis
% * PROC: ajuste_direto
% * DESC: "2083/2020"
% * VALOR: 183.24
% * LOCAL: "Figueira da Foz, Coimbra, Portugal"
% * DATA: 21/02/2020
contrato(22,
         5,
         13,
         'Aquisicao de Bens Moveis',
         'Ajuste Direto',
         '2083/2020',
         183.24,
         prazointer,
         'Figueira da Foz, Coimbra, Portugal',
         (21,2,2020)
).

nuloInterdito(prazointer).

nao(contrato(IDC, ID1, ID2, TIPO, PROC, DESC, VALOR, _, LOCAL, D)) :-
    contrato(IDC, ID1, ID2, TIPO, PROC, DESC, VALOR, prazointer, LOCAL, D).

% É impossivel saber a data exata do contrato saldado entre as entidades 4 e 14
% * TIPO: aquisicao_servicos
% * PROC: concurso_publico
% * DESC: "Assistência Técnica a veículos pesados da marca VOLVO"
% * VALOR: 80000
% * PRAZO: 365
% * LOCAL: "Moita, Setubal, Portugal"
contrato(23,
         4,
         14,
         'Aquisicao de Servicos',
         'Concurso Publico',
         'Assistência Técnica a veículos pesados da marca VOLVO',
         80000,
         365,
         'Moita, Setubal, Portugal',
         datainter
).

nuloInterdito(datainter).

nao(contrato(IDC, ID1, ID2, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, _)) :-
    contrato(IDC, ID1, ID2, TIPO, PROC, DESC, VALOR, PRAZO, LOCAL, datainter).


%------------------------------------------------------------------------------
% A série de invariantes que se seguem têm por base o guia de Contratação
% Pública (https://poise.portugal2020.pt/documents/10180/25525/Anexo_CN.12.UC.U
% AC_Guia_de_contratacao_publica_POISE_Junho+2016_VF.pdf/0933b572-2d0c-4b64-800
% 4-791f6f7de71f).
%
% Estas regras dizem respeito à escolha do procedimento de acordo com o valor
% do contrato.
%------------------------------------------------------------------------------


%------------------------------------------------------------------------------
% Teste se existe apenas um contrato com o providenciado número identificador.
%------------------------------------------------------------------------------

existeApenasContrato(IdAd) :-
    (solucoes((_), (contrato(IdAd, _, _, _, _, _, _, _, _, _)), S),
    comprimento(S, N),
    N == 1).

%------------------------------------------------------------------------------
% AJUSTE DIRETO ---------------------------------------------------------------
%------------------------------------------------------------------------------

+contrato(Id, IdAd, IdAda, _, _, _, _, _, _, _) ::
    (
        existeApenasContrato(Id),
        adjudicanteInKB(IdAd),
        adjudicatariaInKB(IdAda)
    ).

+contrato(
    _,
    _,
    _,
    TipoProc,
    'Ajuste Direto',
    _,
    Valor,
    _,
    _,
    _
        ) ::
        (
                (
                    TipoProc = 'Bens e Servicos',
                    Valor =< 75000
                );
                (
                    TipoProc = 'Empreitada de Obras Publicas',
                    Valor =< 150000
                );
                (
                    TipoProc \= 'Bens e Servicos',
                    TipoProc \= 'Empreitada de Obras Publicas',
                    Valor =< 100000
                )
        ).


%------------------------------------------------------------------------------
% CONCURSO PUBLICO DE AMBITO NACIONAL -----------------------------------------
%------------------------------------------------------------------------------

 +contrato(
     _,
     _,
     _,
     TipoProc,
     'Concurso Publico de Ambito Nacional',
     _,
     Valor,
     _,
     _,
     _
         ) ::
         (
                 (
                     TipoProc == 'Empreitada de Obras Publicas',
                     Valor =< 5225000
                 );
                 (
                     TipoProc == 'Bens e Servicos',
                     Valor =< 418000
                 )
         ).

%------------------------------------------------------------------------------
% INTRODUÇÃO DE CONTRATOS -----------------------------------------------------
% CONDIÇÕES OBRIGATÓRIAS POR AJUSTE DIRETO: -----------------------------------
% * Valor igual ou inferior a 5000 euros; -------------------------------------
% * Contrato de aquisição ou locação de bens móveis ou aquisição de serviços; -
% * Prazo de vigência até 1 ano, inclusive, a contar da decisão da adjudicação;
%------------------------------------------------------------------------------

+contrato(
    _,
    _,
    _,
    TipoContrato,
    'Ajuste Direto Simplificado',
    _,
    Valor,
    Prazo,
    _,
    _
        ) ::
        (
            Valor =< 5000,
            (
                    TipoContrato = 'Aquisicao de Bens Moveis';
                    TipoContrato = 'Locacao de Bens Moveis';
                    TipoContrato = 'Aquisicao de Servicos'
            ),
            Prazo =< 365
        ).

%------------------------------------------------------------------------------
% INTRODUÇÃO DE CONTRATOS -----------------------------------------------------
% REGRA DOS 3 ANOS VÁLIDA PARA TODOS OS CONTRATOS: ----------------------------
% * Uma entidade ajudicante não pode convidar a mesma empresa para celebrar um
% contrato com prestações de serviço do mesmo tipo ou idênticas às de contratos
% que já lhe foram atribuídos, no ano encómico em curso e nos dois anos
% económicos anteriores, sempre que: O preço contratual acumulado dos contratos
% já celebrados (não incluíndo o contrato que se pretende celebrar) seja igual
% ou superior a 75000 euros. --------------------------------------------------
%------------------------------------------------------------------------------

+contrato(
    _,
    IdAd,
    IdAda,
    TipoContrato,
    _,
    _,
    Valor,
    _,
    _,
    Data
        ) ::
        (
            solucoes(
                (V),
                (
                    contrato(_, IdAd, IdAda, TipoContrato, _, _, V, _, _, D),
                    lastThreeYears(D, Data)
                ),
                R),
            sumList(R, S),
            S < 75000 + Valor
        ).


sumList([], 0).
sumList([V|T], R) :-
    sumList(T, SumRes), R is SumRes + V.


calcule_days((Y,M,D), Res):-
    Res is round(((Y*1461)/4)+((M*153)/5)+D).


lastThreeYears((Dia, Mes, Ano), (Diac, Mesc, Anoc)) :-
    calcule_days((Anoc, Mesc, Diac), R),
    calcule_days((Ano, Mes, Dia), Y),
    R - Y < 3 * 365.