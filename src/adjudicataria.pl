% vim: syntax=prolog
%------------------------------------------------------------------------------
% MÓDULO: ADJUDICATÁRIA -------------------------------------------------------
%------------------------------------------------------------------------------

:- include('utils.pl').
:- dynamic adjudicataria/4.

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ---------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- discontiguous adjudicataria/4.
:- discontiguous excecao/1.
:- discontiguous nuloIncerto/1.
:- discontiguous (::)/2.
:- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Representação de conhecimento positivo --------------------------------------
%------------------------------------------------------------------------------

adjudicataria( 1,
               'RACLAC, LDA',
               507992822,
               'Portugal'
             ).

adjudicataria( 2,
               'Porlagmedia - Edicao e Distribuicao, LDA',
               513023801,
               'Portugal'
             ).

adjudicataria( 3,
               'ULTRASONO RADIOLOGIA, LDA',
               502818093,
               'Portugal'
             ).

adjudicataria( 4,
               'Certoma - Comercio Tecnico de Maquinas, LDA',
               501777407,
               'Portugal'
             ).

%------------------------------------------------------------------------------
% Representação de conhecimento negativo --------------------------------------
%------------------------------------------------------------------------------

-adjudicataria( 1,
                'C.M.Babo - Auditoria & Gestao, Lda',
                504201190,
                'Portugal'
              ).

-adjudicataria( 2,
                'Medicinalia Cormedica, Lda',
                500684324,
                'Portugal'
              ).

-adjudicataria( 3,
                'Hospital Lusiadas, S.A',
                505962403,
                'Portugal'
              ).

-adjudicataria( 4,
                'Pierre fabre Dermo Cosmetique Portugal, Lda',
                501757635,
                'Portugal'
              ).

%------------------------------------------------------------------------------
% Definição de conhecimento negativo ------------------------------------------
%------------------------------------------------------------------------------

-adjudicataria(IdAda, Nome, NIF, Morada) :-
    nao(adjudicataria(IdAda, Nome, NIF, Morada)),
    nao(excecao(adjudicataria(IdAda, Nome, NIF, Morada))).

%------------------------------------------------------------------------------
% IMPRECISO -------------------------------------------------------------------
%------------------------------------------------------------------------------

% Não é conhecido o NIF da "Overpharma". Sabemos apenas que está contido entre
% 505500000 e 505900000.
excecao(
    adjudicataria(11,
                 'Overpharma',
                 NIF,
                 'Portugal'
                 )
        ) :- NIF =< 505900000, NIF >=505500000.

% Não é conhecido o nome da entidade adjudicataria de id = 12. Sabemos que é um
% dos seguintes:
% * "ESSITY PORTUGAL, LDA"
% * "ESSITY PORTUGAL"
% * "ESSITY"
excecao(
    adjudicataria(12,
                  'ESSITY PORTUGAL, LDA',
                  503237612,
                  'Avenida D. João II, Vila Nova de Gaia, Portugal'
                  )
        ).

excecao(
    adjudicataria(12,
                  'ESSITY PORTUGAL',
                  503237612,
                  'Avenida D. João II, Vila Nova de Gaia, Portugal'
                  )
        ).

excecao(
    adjudicataria(12,
                  'ESSITY',
                  503237612,
                  'Avenida D. João II, Vila Nova de Gaia, Portugal'
                  )
        ).

% Não é conhecido o NIF da "Alvarsol, Lda". Sabemos apenas que não é: 503148709
excecao(
    adjudicataria(13,
                  'Alvarsol, Lda',
                  NIF,
                  'Quarteira, Faro, Portugal'
                  )
        ) :- NIF \= 503148709.

% O id da entidade adjudicataria "João Antunes Amaro, Lda" é desconhecido, mas
% sabemos que está contido entre 93 e 101 ou 173 e 185.
excecao(
    adjudicataria(ID,
                  'João Antunes Amaro, Lda',
                  500149003,
                  'Portugal'
                  )
        ) :- ID =< 101 , ID >= 93;
             ID =< 173 , ID >=185.

% Não é conhecido o nome nem morada exatos da entidade adjudicataria com id =
% 14. Sabemos que o nome é um dos seguintes:
% * "B. BRAUN MEDICAL, LDA"
% * "JOHNSON & JOHNSON MEDICAL"
% * o Nif é 501506543 ou 500153370
excecao(
    adjudicataria(
        14,
        'B. BRAUN MEDICAL, LDA',
        501506543,
        'Lisboa,Portugal'
                 )
       ).

excecao(
    adjudicataria(
        14,
        'B. BRAUN MEDICAL, LDA',
        500153370,
        'Lisboa,Portugal'
                )
        ).

excecao(
    adjudicataria(
        14,
        'JOHNSON & JOHNSON MEDICAL',
        501506543,
        'Lisboa,Portugal'
                )
       ).

excecao(
    adjudicataria(
        14,
        'JOHNSON & JOHNSON MEDICAL',
        500153370,
        'Lisboa,Portugal'
                )
        ).


%------------------------------------------------------------------------------
% INCERTO ---------------------------------------------------------------------
%------------------------------------------------------------------------------


% Não é conhecido o Nome da entidade adjudicataria de id=15.
% NIF:	980166705
% MORADA: "Portugal"
adjudicataria(15, nomedesc, 980166705, 'Portugal').
nuloIncerto(nomedesc).
excecao(adjudicataria(ID, _, NIF, MORADA)) :-
    adjudicataria(ID, nomedesc, NIF, MORADA).


% Não é conhecido o NIF da entidade adjudicataria com id = 16.
% NOME: "Teva".
% MORADA:"Portugal".
adjudicataria(16, 'Teva', nifdesc, 'Portugal').
nuloIncerto(nifdesc).
excecao(adjudicataria(ID, NOME, _, MORADA)) :-
    adjudicataria(ID, NOME, nifdesc, MORADA).

% Não é conhecida a morada da entidade adjudicataria de id = 17.
% NOME: "KNOW-HOW, LDA."
% NIF: 509884296
adjudicataria(17, 'KNOW-HOW, LDA.', 509884296, moradadesc).
nuloIncerto(moradadesc).
excecao(adjudicataria(ID, NOME, NIF, _)) :-
    adjudicataria(ID, NOME, NIF, moradadesc).

% Não sabemos o id da entidade adjudicataria de nome "Seguradoras Unidas, S.A."
% NIF: 500940231
% MORADA: "Portugal"
adjudicataria(iddesc, 'Seguradoras Unidas, S.A.', 500940231, 'Portugal').
nuloIncerto(iddesc).
excecao(adjudicataria(_, NOME, NIF, MORADA)) :-
    adjudicataria(iddesc, NOME, NIF, MORADA).

%------------------------------------------------------------------------------
% INVARIANTES -----------------------------------------------------------------
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
% INTRODUÇÃO DE ADJUDICATÁRIAS ------------------------------------------------
% Não se podem introduzir adjudicatárias com campos _IdAda_ ,_Nome_ ou _NIF_ que
% já constem da base de conhecimento. -----------------------------------------
%------------------------------------------------------------------------------

+adjudicataria(IdAda, _, _, _) ::
    (solucoes((IdAda), (adjudicataria(IdAda, _, _, _)), S),
    comprimento(S, N),
    N =< 1).

+adjudicataria(_, Nome, _, _) ::
    (solucoes((Nome), (adjudicataria(_, Nome, _, _)), S),
    comprimento(S, N),
    N =< 1).

+adjudicataria(_, _, NIF, _) ::
    (solucoes((NIF), (adjudicataria(_, __, NIF, _)), S),
    comprimento(S, N),
    N =< 1).

%------------------------------------------------------------------------------
% REMOÇÃO DE ADJUDICATÁRIAS ---------------------------------------------------
% Não é possível remover adjudicatárias para os quais existam contratos. ------
%------------------------------------------------------------------------------

-adjudicataria(IdAda, _, _, _) ::
    (solucoes((IdAda), contrato(_, IdAda, _, _, _, _, _, _, _, _), S),
    comprimento(S, N),
    N == 0).

adjudicatariaInKB(IdAda) :-
    adjudicataria(IdAda, _, _, _).