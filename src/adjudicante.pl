% vim: syntax=prolog
%------------------------------------------------------------------------------
% FICHEIRO: ADJUDICANTE -------------------------------------------------------
%------------------------------------------------------------------------------

:- include('utils.pl').
:- dynamic adjudicante/4.

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ---------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- discontiguous adjudicante/4.
:- discontiguous excecao/1.
:- discontiguous nuloIncerto/1.
:- discontiguous nuloInterdito/1.
:- discontiguous nao/1.
:- discontiguous (::)/2.
:- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Representação de conhecimento positivo --------------------------------------
%------------------------------------------------------------------------------

adjudicante( 1,
             'Administracao Regional de Saude do Norte',
             503135593,
             'Portugal, Porto, Porto'
           ).

adjudicante( 2,
             'Camara Municipal de Portimao',
             505309939,
             'Portugal, Faro, Portimao'
           ).

adjudicante( 3,
             'Centro Hospitalar do Oeste, E.P.E',
             514993871,
             'Portugal, Leiria, Caldas da Rainha'
           ).

adjudicante( 4,
             'Uniao das Freguesias de Alcacer do Sal',
              501777407,
             'Portugal'
           ).

%------------------------------------------------------------------------------
% Representação de conhecimento negativo --------------------------------------
%------------------------------------------------------------------------------

-adjudicante( 1,
              'Municipio de Amarante',
              501102752,
              'Portugal, Porto, Amarante'
            ).

-adjudicante( 2,
              'Hospital Distrital de Santarem, E. P. E',
              506361462,
              'Portugal, Santarem, Santarem'
           ).

-adjudicante( 3,
              'Municipio de Viseu',
              506697320,
              'Portugal, Viseu, Viseu'
            ).

-adjudicante( 4,
              'Unidade Local de Saude de Matosinhos, E. P. E',
              506361390,
              'Portugal, Porto, Matosinhos'
            ).

%------------------------------------------------------------------------------
% DEFINIÇÃO DE CONHECIMENTO NEGATIVO ------------------------------------------
%------------------------------------------------------------------------------

-adjudicante(IdAd, Nome, NIF, Morada) :-
    nao(adjudicante(IdAd, Nome, NIF, Morada)),
    nao(excecao(adjudicante(IdAd, Nome, NIF, Morada))).

%------------------------------------------------------------------------------
% IMPRECISO -------------------------------------------------------------------
%------------------------------------------------------------------------------

% Não sabemos o NIF exato do "Centro Hospitalar e Universitário de Coimbra", de
% id=5, sabemos apenas que está entre 123000000 e 126000000.
excecao(
    adjudicante(5,
                'Centro Hospitalar e Universitario de Coimbra',
                NIF,
                'Coimbra, Portugal'
                )
        ) :- NIF >= 123000000, NIF =< 126000000.


% Não é conhecido o nome exato do adjudicante de id=6 usado na base de
% conhecimento. Sabemos, no entanto, que é um dos seguintes:
% * "Aguas do Norte"
% * "Aguas do Norte, S.A."
% * "Agu. Norte, S.A"
excecao(adjudicante(6, 'Aguas do Norte', 513606084, 'Vila Real, Portugal')).
excecao(adjudicante(6, 'Aguas do Norte, S.A.', 513606084, 'Vila Real, Portugal')).
excecao(adjudicante(6, 'Agu. Norte, S.A', 513606084, 'Vila Real, Portugal')).


% Não é conhecida a morada do adjudicante de id=7, "Municipio de Arganil" com
% NIF 506833232.
% A ultima morada conhecida já não é correta, pelo que a unica coisa que
% sabemos é que a morada não é:
% * "Lisboa, Lisboa, Portugal"
excecao(
    adjudicante(7,
                'Municipio de Arganil',
                506833232,
                MORADA
                )
        ) :- MORADA \= 'Lisboa, Lisboa, Portugal'.


% O id do adjudicante do "Municipio de Aveiro", com NIF 505931192, está entre
% 84 e 103 ou entre 107 e 123, não sendo conhecido o valor exato.
excecao(
    adjudicante(ID,
                'Municipio de Aveiro',
                505931192,
                'Aveiro, Portugal'
                )
        ) :- ID >= 84 , ID =< 103;
             ID >= 107 , ID =< 123.


% Não é conhecido o id e nif exatos do "Município de Setúbal". Sabemos que o id
% é 8 ou 88, e que o Nif é 501294104 ou 129410450.
excecao(adjudicante(8, 'Município de Setúbal', 501294104, 'Setubal, Portugal')).
excecao(adjudicante(8, 'Município de Setúbal', 129410450, 'Setubal, Portugal')).
excecao(adjudicante(88, 'Município de Setúbal', 501294104, 'Setubal, Portugal')).
excecao(adjudicante(88, 'Município de Setúbal', 129410450, 'Setubal, Portugal')).

%------------------------------------------------------------------------------
% INCERTO ---------------------------------------------------------------------
%------------------------------------------------------------------------------

% Não sabemos o NIF da entidade adjudicante de id = 9.
% Nome: "Centro Hospitalar Universitário do Algarve, E. P. E."
% Morada: "Algarve, Portugal"
adjudicante(
    9,
    'Centro Hospitalar Universitario do Algarve',
    nifdesc,
    'Algarve, Portugal'
           ).

nuloIncerto(nifdesc).

excecao(
    adjudicante(
                ID,
                NOME,
                _,
                MORADA
               )
       ) :- adjudicante(ID, NOME, nifdesc, MORADA).

% Não é conhecido o nome da entidade adjudicante de id = 10.
% NIF 503657190.
% MORADA:"Sintra,Lisboa,Portugal"
adjudicante(10, nomedesc, 503657190, 'Sintra,Lisboa,Portugal').

nuloIncerto(nomedesc).

excecao(adjudicante(ID, _, NIF, MORADA )) :-
    adjudicante(ID, nomedesc, NIF, MORADA).

% Não é conhecida a morada do adjudicante de id = 11.
% Nome: "Instituto da Mobilidade e dos Transportes".
% NIF: 508195446.
adjudicante(
    11,
    'Instituto da Mobilidade e dos Transportes',
    508195446,
    moradadesc
           ).

nuloIncerto(moradadesc).

excecao(adjudicante(ID, NOME, NIF, _)) :-
    adjudicante(ID, NOME, NIF, moradadesc).

% Não é conhecido o ID da entidade adjudicante de nome "Instituto Nacional de
% Saude Doutor Ricardo Jorge".
% NIF: 501427511
% MORADA: "Lisboa, Lisboa, Portugal"
adjudicante(
    iddesc,
    'Instituto Nacional de Saude Doutor Ricardo Jorge',
    501427511,
    'Lisboa, Lisboa, Portugal'
           ).

nuloIncerto(iddesc).

excecao(adjudicante(_, NOME, NIF, MORADA)) :-
    adjudicante(iddesc, NOME, NIF, MORADA).

%------------------------------------------------------------------------------
% INTERDITO -------------------------------------------------------------------
%------------------------------------------------------------------------------


% É impossivel saber a morada da entidade adjudicante com id = 13
% * Nome: "Instituto Nacional de Estatística, I. P."
% * NIF: 502237490
adjudicante(
    13,
    'Instituto Nacional de Estatistica, I. P.',
    502237490,
    moradainter
           ).

nuloInterdito(moradainter).

nao(adjudicante(ID, NOME, NIF, _)) :-
    adjudicante(ID, NOME, NIF, moradainter).

% É impossivel saber o nome da entidade adjudicante com id = 14
% * NIF:503148768
% * MORADA:"Evora, Evora, Portugal"
adjudicante(14, nomeinter, 503148768, 'Evora, Evora, Portugal').

nuloInterdito(nomeinter).

nao(adjudicante(ID, _, NIF, MORADA)) :-
    adjudicante(ID, nomeinter, NIF, MORADA).

% É impossivel saber o NIF da entidade adjudicante com id = 15
% * Nome: "Municipio de Loule"
% * MORADA:"Loule, Faro, Portugal"
adjudicante(15, 'Municipio de Loule', nifinter, 'Loule, Faro, Portugal').

nuloInterdito(nifinter).

nao(adjudicante(ID, NOME, _, MORADA)) :-
    adjudicante(ID, NOME, nifinter, MORADA).

% É impossivel saber o id da entidade adjudicante com nome "Centro de Formação
% Profissional da Indústria de Calçado".
% * NIF:900106590
% * MORADA:"Sao Joao da Madeira, Aveiro, Portugal"
adjudicante(
    idinter,
    'Centro de Formação Profissional da Indústria de Calçado',
    900106590,
    'Sao Joao da Madeira, Aveiro, Portugal'
).

nuloInterdito(idinter).

nao(adjudicante(_, NOME, NIF, MORADA)) :-
    adjudicante(idinter, NOME, NIF, MORADA).

%------------------------------------------------------------------------------
% INVARIANTES -----------------------------------------------------------------
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
% INTRODUÇÃO DE ADJUDICANTES --------------------------------------------------
% Não se podem introduzir adjudicantes com campos _IdAd_, _Nome_ ou _NIF_ que
% já constam da base de conhecimento. -----------------------------------------
%------------------------------------------------------------------------------

+adjudicante(IdAd, _, _, _) ::
    (solucoes((IdAd), adjudicante(IdAd, _, _, _), S),
    comprimento(S, N),
    N =< 1).

+adjudicante(_, Nome, _, _) ::
    (solucoes((Nome), adjudicante(_, Nome, _, _), S),
    comprimento(S, N),
    N =< 1).

+adjudicante(_, _, NIF, _) ::
    (solucoes((NIF), adjudicante(_, _, NIF, _), S),
    comprimento(S, N),
    N =< 1).

%------------------------------------------------------------------------------
% REMOÇÃO DE ADJUDICANTES -----------------------------------------------------
% Não é possível remover adjudicantes para os quais existem contratos. --------
%------------------------------------------------------------------------------

-adjudicante(IdAd, _, _, _) ::
    (solucoes((IdAd), (contrato(IdAd, _, _, _, _, _, _, _, _, _)), S ),
    comprimento(S, N),
    N == 0).

adjudicanteInKB(IdAd) :-
    adjudicante(IdAd, _, _, _).
