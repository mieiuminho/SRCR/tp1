% vim: syntax=prolog
%------------------------------------------------------------------------------
% FICHEIRO: UTILITÁRIOS -------------------------------------------------------
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ----------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Permite saber se um termo consta ou não da base de conhecimento. ------------
%------------------------------------------------------------------------------

nao(P) :-
    P, !, fail;
    true.

%------------------------------------------------------------------------------
% SISTEMA DE INFERÊNCIA -------------------------------------------------------
%------------------------------------------------------------------------------

demo(P, verdadeiro) :-
    P,
    nao(excecao(P)),
    nao(nao(P)).
demo(P, desconhecido) :-
    excecao(P).
demo(P, falso) :-
    nao(P),
    nao(excecao(P));
    -P.

%------------------------------------------------------------------------------
% ENCAPSULAMENTO DE MÉTODOS JÁ EXISTENTES -------------------------------------
%------------------------------------------------------------------------------

solucoes(X, Y, Z) :-
    findall(X, Y, Z).

comprimento(S, N) :-
    length(S, N).

%------------------------------------------------------------------------------
% Permite a evolução de conhecimento, respeitando os invariantes. -------------
%------------------------------------------------------------------------------

evolucao(Termo) :-
    solucoes(Invariante, +Termo::Invariante, Lista),
    insercao(Termo),
    teste(Lista).

adicionar(Termo) :-
    evolucao(Termo).

involucao(Termo) :-
    solucoes(Invariante, -Termo::Invariante, Lista),
    remocao(Termo),
    teste(Lista).

remover(Termo) :-
    involucao(Termo).

insercao(Termo) :-
    assert(Termo).
insercao(Termo) :-
    retract(Termo), !, fail.

remocao(Termo) :-
    retract(Termo).
remocao(Termo) :-
    assert(Termo), !, fail.

teste([]).
teste([R|LR]) :-
    R,
    teste(LR).

%------------------------------------------------------------------------------
% INVARIANTE respetivo à introdução de exceções. ------------------------------
%------------------------------------------------------------------------------

+excecao(Termo) ::
    (nao(Termo),
    solucoes(Termo, excecao( Termo ), S),
    comprimento(S, N),
    N =< 1).